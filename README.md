# Lazy one-time initialization

It's like `std::sync::Once`, but holds the result. It's like `lazy_static!`, but works for non-static values.

It's a tiny helper library used to initialize some computed properties in crates.rs projects without having to make the method calls mutable.
